# Project Name: Green Leaf E-Commerce API

## Regular User Credentials:

email: "user@mail.com"
password: "user1234"

## Admin Credentials:

email: "admin@mail.com"
password: "admin1234"

## Features:
- User registration
- User authentication
- Set user as admin (Admin only)
- Retrieve all active products
- Retrieve single product
- Retrieve all products
- Create Product (Admin only)
- Update Product information (Admin only)
- Archive Product (Admin only)
- Make Product Available (Admin only)
- Non-admin User checkout (Create Order)
- Retrieve authenticated user’s orders
- Retrieve all orders (Admin only)
- Retrieve User Details
- Check if Email Exists

## Other Features:
- Add to Cart
	- Added Products
	- Change product quantities
	- Remove products from cart
	- Subtotal for each item
	- Total price for all items
