//dependencies
const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();

//allows all resources to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//datbaseconnections
	//connecting to mongodb atlas

mongoose.set('strictQuery', true);
//cahngethis line21
mongoose.connect("mongodb+srv://admin:bautista1234@clusterbatch248.jeupjjp.mongodb.net/greenleaf-ecommerce-api?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//connection to databse
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"
	));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"))

//defines the users string to be includesd forall user routes defined in the userRoutes.js
app.use("/users", userRoutes);


//defines the users string to be includesd forall user routes defined in the userRoutes.js
app.use("/products", productRoutes);

app.use("/orders", orderRoutes);

app.listen(process.env.PORT || 4000, ()=>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})